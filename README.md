# Application

Try with this URL: [http://35.198.232.167](http://35.198.232.167)

Barnch  
1. master: Final source  
2. req-0: React rutorial  
3. req-1: Input name  
4. req-2: Change color  
5. req-3: Show Draw  
6. req-other: Other changes such as styles  

# tic-tac-toe

Request về bài tập sẽ có 2 option.

Đối với cả 2 option đều phải up source code lên Bitbucket, sharing để phía công ty có thể access vào tham khảo.

## Option 1: Source code về ReactJS
Theo hướng dẫn của tutorial React.js(https://reactjs.org/tutorial/tutorial.html):

(a) Làm theo tutorial và hoàn thành cả 3 bước của nó

(i) Bên cạnh đó, vui lòng thực hiện 3 việc sau:

       (*) Cả hai người chơi đều có thể nhập tên người chơi một cách tự do và hiển thị giá trị đã nhập vào thời điểm bắt đầu chơi.

       (*) Thay đổi màu của “X” và ”O”（”X”→#000080、”O”→#b22222）

       (*) Nếu trường hợp đã điền đủ 9 ô, nếu kết quả là hòa, hiện ra “draw”, nếu một trong 2 người chơi thắng thì hiển thị ra [tên đã nhập ở mục (1) + Win!!] (Ví dụ “I Win!!”)

(u) Nếu bạn cảm thấy tự tin(và còn nhiều thời gian), thì có thể tự cải tiến về chức năng, UI, UX, thuật toán. 

Note: Nên giải thích khi gửi mail để show cho họ biết chỉnh sửa những gì, làm thế nào?

## Option 2: 
Có thể dùng bất kỳ source code nào của Java Script mà mình đã từng tự coding mà cảm thấy tâm đắt (AngularJS, ReactJS...) để propose.

### Req 1

Add new compenent to input name

```javascript
class NameForm extends React.Component {
    constructor(props) {
        super(props);
        console.log(props.value);
        this.state = { value: props.value, index: props.index, onChange: props.onChange };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
        this.state.onChange(this.state.index, event.target.value);
    }

    render() {
        return (
            <form>
                <label>
                    Name {this.state.index}:
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
            </form>
        );
    }
}
```

### Req 2

Change color X and O

Update class name of button

```html
className={"square " + "square-" + props.value}
```

Add two CSS classes 

```css
.square.square-X {
    color: #000080;
}

.square.square-O {
    color: #b22222;
}
```

### Req 3

Add function check can continue

```javascript
function checkCanContinue(squares) {
    return squares.includes(null);
}
```

Change status if draw

```javascript
if (!checkCanContinue(current.squares)) {
    status = "draw";
}
```

### Other

Change style css for table tic-tac-toe

Update function handle click

```javascript
if (squares[i] || calculateWinner(squares)) {
    return;
}
```

Moving ```squares[i]``` can increase performance for program