import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
    return (
        <button className={"square " + "square-" + props.value} onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    renderSquare(i) {
        return (
            <Square
                value={this.props.squares[i]}
                onClick={() => this.props.onClick(i)}
            />
        );
    }

    render() {
        return (
            <table id="board">
                <tbody>
                    <tr>
                        <td>
                            {this.renderSquare(0)}
                        </td>
                        <td>
                            {this.renderSquare(1)}
                        </td>
                        <td>
                            {this.renderSquare(2)}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {this.renderSquare(3)}
                        </td>
                        <td>
                            {this.renderSquare(4)}
                        </td>
                        <td>
                            {this.renderSquare(5)}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {this.renderSquare(6)}
                        </td>
                        <td>
                            {this.renderSquare(7)}
                        </td>
                        <td>
                            {this.renderSquare(8)}
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

class NameForm extends React.Component {
    constructor(props) {
        super(props);
        console.log(props.value);
        this.state = { value: props.value, index: props.index, onChange: props.onChange };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
        this.state.onChange(this.state.index, event.target.value);
    }

    render() {
        return (
            <form>
                <label>
                    Name {this.state.index}:
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
            </form>
        );
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [
                {
                    squares: Array(9).fill(null)
                }
            ],
            stepNumber: 0,
            xIsNext: true,
            playerOne: 'Player 1',
            playerTwo: 'Player 2'
        };
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (squares[i] || calculateWinner(squares)) {
            return;
        }
        squares[i] = this.state.xIsNext ? "X" : "O";
        this.setState({
            history: history.concat([
                {
                    squares: squares
                }
            ]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext
        });
    }

    updateName(i, value) {
        if (i === 1) this.setState({ playerOne: value })
        if (i === 2) this.setState({ playerTwo: value })
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0
        });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);

        const moves = history.map((step, move) => {
            const desc = move ?
                'Go to move #' + move :
                'Restart game';
            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        let status;
        if (winner) {
            status = (this.state.xIsNext ? this.state.playerTwo : this.state.playerOne) + ' Win!!'
        } else {
            status = 'Turn: ' + (this.state.xIsNext ? 'X' : 'O') + ' (' + (this.state.xIsNext ? this.state.playerOne : this.state.playerTwo) + ')';
        }

        if (!checkCanContinue(current.squares)) {
            status = "X O Draw!";
        }

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={i => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>
                        <NameForm
                            index={1} value={this.state.playerOne}
                            onChange={(i, value) => this.updateName(i, value)}></NameForm>
                    </div>
                    <div>
                        <NameForm
                            index={2} value={this.state.playerTwo}
                            onChange={(i, value) => this.updateName(i, value)}></NameForm>
                    </div>
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(<Game />, document.getElementById("root"));

function checkCanContinue(squares) {
    return squares.includes(null);
}

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}
